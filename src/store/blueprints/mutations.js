import ArrayHelper from '../../libs/array-helper'

/**
 * @param state
 * @param blueprint
 */
export function setSelectBlueprint (state, blueprint) {
  state.select.blueprint = blueprint
}

/**
 * @param state
 * @param blueprints
 */
export function setBlueprints (state, blueprints) {
  state.blueprints = blueprints
}

/**
 * @param state
 * @param blueprint
 */
export function addBlueprints (state, blueprint) {
  state.blueprints.push(blueprint)
}

/**
 * Supprime un blueprint
 * @param state
 * @param id
 */
export function removeBlueprint (state, { id }) {
  const blueprint = state.blueprints.find(b => b.id === id)
  ArrayHelper.remove(state.blueprints, blueprint)
}

/**
 * @param state
 * @param blueprints
 */
export function updateBlueprints (state, blueprints) {
  ArrayHelper.update(state.blueprints, blueprints)
}

/**
 * @param state
 * @param blueprint
 */
export function upsertBlueprintItem (state, blueprint) {
  const bp = state.blueprints.find(b => b.id === blueprint.id) || {}

  bp.id = blueprint.id
  bp.type = blueprint.type
  bp.name = blueprint.name
  bp.desc = blueprint.desc
  bp.tag = blueprint.tag
  bp.weight = blueprint.weight

  bp.subtype = blueprint.subtype
  bp.uname = blueprint.uname
  bp.udesc = blueprint.udesc

  bp.stackable = blueprint.stackable
  bp.identified = blueprint.identified
  bp.inventory = blueprint.inventory
  bp.lock = blueprint.lock
  bp.buc = blueprint.buc

  const index = state.blueprints.indexOf(bp)
  if (index >= 0) {
    ArrayHelper.setElement(state.blueprints, index, bp)
  } else {
    state.blueprints.push(bp)
  }
}

/**
 * @param state
 * @param blueprints
 */
export function upsertBlueprintPlaceable (state, blueprints) {
  const bp = state.blueprints.find(b => b.id === blueprints.id) || {}

  bp.id = blueprints.id
  bp.type = blueprints.type
  bp.name = blueprints.name
  bp.desc = blueprints.desc
  bp.tag = blueprints.tag
  bp.weight = blueprints.weight

  bp.inventory = blueprints.inventory
  bp.lock = blueprints.lock
  bp.traps = blueprints.traps

  const index = state.blueprints.indexOf(bp)
  if (index >= 0) {
    ArrayHelper.setElement(state.blueprints, index, bp)
  } else {
    state.blueprints.push(bp)
  }
}

/**
 * @param state
 * @param blueprints
 */
export function upsertBlueprintActor (state, blueprints) {
  const bp = state.blueprints.find(b => b.id === blueprints.id) || {}

  bp.id = blueprints.id
  bp.type = blueprints.type
  bp.name = blueprints.name
  bp.desc = blueprints.desc
  bp.tag = blueprints.tag
  bp.weight = blueprints.weight

  bp.race = blueprints.race
  bp.gender = blueprints.gender
  bp.rank = blueprints.rank

  bp.inventory = blueprints.inventory

  const index = state.blueprints.indexOf(bp)
  if (index >= 0) {
    ArrayHelper.setElement(state.blueprints, index, bp)
  } else {
    state.blueprints.push(bp)
  }
}
