export const getRightPanel = state => state.dynamic.rightPanel
export const getTypeBlueprintSelect = state => state.dynamic.typeBlueprintSelect
export const getDialogBlueprint = state => state.dynamic.dialogBlueprint
export const getDialogImport = state => state.dynamic.dialogImport
