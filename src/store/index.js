import Vue from 'vue'
import Vuex from 'vuex'
import map from './map'
import blueprints from './blueprints'
import mudEditor from './mudEditor'
import tech from './tech'
import localStoragePlugin from 'src/store/plugins/lsPlugin'

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      map,
      blueprints,
      mudEditor,
      tech
    },
    plugins: [
      localStoragePlugin
    ],
    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  })

  return Store
}
