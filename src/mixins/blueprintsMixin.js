import { createNamespacedHelpers } from 'vuex'
import { exportFile } from 'quasar'

const {
  mapMutations: blueprintsMapMutations,
  mapActions: blueprintsMapActions,
  mapGetters: blueprintsMapGetters
} = createNamespacedHelpers('blueprints')

export default {
  computed: {
    ...blueprintsMapGetters([
      'getRefTypes',
      'getRefSubtypes',
      'getRefRaces',
      'getRefGenders',
      'getRefRanks',
      'getBlueprints',
      'getTreeBlueprints',
      'getSelectBlueprint'
    ]),
    selectBlueprint: {
      get: function () {
        return this.getSelectBlueprint
      },
      set: function (blueprint) {
        this.setSelectBlueprint(blueprint)
      }
    }
  },
  methods: {
    ...blueprintsMapActions([
      'submitItem',
      'submitPlaceable',
      'submitActor',
      'saveBlueprints',
      'loadBlueprints'
    ]),
    ...blueprintsMapMutations([
      'setSelectBlueprint',
      'updateBlueprints',
      'removeBlueprint'
    ]),
    exportBlueprint ({ id }) {
      const blueprint = this.getBlueprints.find(b => b.id === id)
      if (blueprint) {
        exportFile(blueprint.name.replaceAll(' ', '_'), JSON.stringify(blueprint, null, '  '))
      }
    },
    exportBlueprints () {
      exportFile('blueprints.json', JSON.stringify(this.getBlueprints, null, '  '))
    },
    async save () {
      await this.saveBlueprints()
      this.$q.notify({
        message: this.$t('SAVE_BP'),
        color: 'green-10',
        textColor: 'grey-5',
        position: 'top'
      })
    },
    async load () {
      await this.loadBlueprints()
      this.$q.notify({
        message: this.$t('LOAD_BP'),
        color: 'green-10',
        textColor: 'grey-5',
        position: 'top'
      })
    }
  }
}
