import { createNamespacedHelpers } from 'vuex'

const {
  mapMutations: techMapMutations,
  mapActions: techMapActions,
  mapGetters: techMapGetters
} = createNamespacedHelpers('tech')

export default {
  computed: {
    ...techMapGetters([
      'getRightPanel',
      'getTypeBlueprintSelect',
      'getDialogBlueprint',
      'getDialogImport',
      'getSupportedLang'
    ]),
    rightPanel: {
      get: function () {
        return this.getRightPanel
      },
      set: function (rightPanel) {
        this.setRightPanel(rightPanel)
      }
    },
    typeBlueprintSelect: {
      get: function () {
        return this.getTypeBlueprintSelect
      },
      set: function (typeBlueprintSelect) {
        this.setTypeBlueprintSelect(typeBlueprintSelect)
      }
    },
    dialogBlueprint: {
      get: function () {
        return this.getDialogBlueprint
      },
      set: function (dialogBlueprint) {
        this.setDialogBlueprint(dialogBlueprint)
      }
    },
    dialogImport: {
      get: function () {
        return this.getDialogImport
      },
      set: function (dialogImport) {
        this.setDialogImport(dialogImport)
      }
    }
  },
  methods: {
    ...techMapActions([]),
    ...techMapMutations([
      'setRightPanel',
      'setTypeBlueprintSelect',
      'setDialogBlueprint',
      'setDialogImport'
    ])
  }
}
